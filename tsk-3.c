#define _USE_MATH_DEFINES
#include <stdio.h>;
#include <math.h>;


int main()
{
    float value;
	char what;
	puts("Enter (xxD / xxR):");
	scanf("%f%c", &value, &what);
	if(what=='R')
		printf("%.2fR = %.2fD\n", value, value*(180/M_PI));
	else 
		printf("%.2fD = %.3fR\n", value, value*(M_PI/180));	
	return 0;
}