#include <stdio.h>

int main()
{
	int hour;
	puts("How much time? (HH:MM)");
	scanf("%d:", &hour);
	if(hour>23) 
		puts("Incorrect format");
	if(hour>=0 && hour<6)
		puts("Good night!");
	else
		if(hour>=6 && hour<12)
			puts("Good morning!");
		else
			if(hour>=12 && hour<18) 
				puts("Good afternoon!");
			else 
				puts("Good evening!");
	return 0;
}